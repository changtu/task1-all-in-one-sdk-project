package com.task1.model;

/**
 * Created by jpotts, Metaversant on 4/11/18.
 */
public interface Task1Model {

    // Content Models
    public static final String NAMESPACE_TASK1_CONTENT_MODEL  = "http://www.task1.com/model/content/1.0";

    // Types
//    public static final String TYPE_TASK1_DOC = "doc";
//    public static final String TYPE_TASK1_FOLDER = "folder";

    // Aspects
    public static final String ASPECT_TASK1_FILES_COUNT = "subfiles";
    public static final String ASPECT_TASK1_FOLDERS_COUNT = "subfolders";

    // Properties
    public static final String PROP_PRODUCT = "product";
    public static final String PROP_VERSION = "version";
    public static final String PROP_PUBLISHED = "published";
    public static final String PROP_IS_ACTIVE = "isActive";

    // Associations
    public static final String ASSN_RELATED_DOCUMENTS = "relatedDocuments";
}
