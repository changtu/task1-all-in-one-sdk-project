package com.task1.schedule.job;

import com.task1.schedule.action.ScheduledJobExecuter;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.schedule.AbstractScheduledLockedJob;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScheduledJob extends AbstractScheduledLockedJob implements StatefulJob {
    private static final Logger LOG = LoggerFactory.getLogger(ScheduledJob.class);

    @Override
    public void executeJob(JobExecutionContext context) throws JobExecutionException {

//        LOG.info("ScheduledJob.executeJob():", context.getJobDetail().getKey().getName());
//        LOG.info("ScheduledJob.executeJob():", context.getJobDetail().getKey());
//        LOG.info("ScheduledJob.executeJob():", context.getJobDetail().getJobDataMap());
//        LOG.info("ScheduledJob.executeJob():", context.getScheduler());
//        LOG.info("ScheduledJob.executeJob() context:", context);

        JobDataMap jobData = context.getJobDetail().getJobDataMap();

        // Extract the Job executer to use
        Object executerObj = jobData.get("jobExecuter");
        if (executerObj == null || !(executerObj instanceof ScheduledJobExecuter)) {
            throw new AlfrescoRuntimeException(
                    "ScheduledJob data must contain valid 'Executer' reference");
        }

        final ScheduledJobExecuter jobExecuter = (ScheduledJobExecuter) executerObj;

        AuthenticationUtil.runAs(new AuthenticationUtil.RunAsWork<Object>() {
            public Object doWork() throws Exception {
                jobExecuter.execute();
                return null;
            }
        }, AuthenticationUtil.getSystemUserName());
    }
}