/**
 * Copyright (C) 2015 Alfresco Software Limited.
 * <p/>
 * This file is part of the Alfresco SDK Samples project.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.task1.schedule.action;

import com.task1.model.Task1Model;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.dictionary.DictionaryService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A simple Repository Action implementation.
 *
 * @author martin.bergljung@alfresco.com
 */
public class SimpleRepoActionExecuter extends ActionExecuterAbstractBase {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleRepoActionExecuter.class);

    public static final String PARAM_SIMPLE = "simpleParam";
    private static final String PARAM_FILE_COUNT = "filesCount";
    private static final String PARAM_FOLDER_COUNT = "foldersCount";

    /**
     * The Alfresco Service Registry that gives access to all public content services in Alfresco.
     */
    private ServiceRegistry serviceRegistry;
    private DictionaryService dictionaryService;
    private NodeService nodeService;
    class Counts {
        int filesCount;
        int foldersCount;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDictionaryService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        paramList.add(new ParameterDefinitionImpl(
                PARAM_SIMPLE,
                DataTypeDefinition.TEXT,
                true,
                getParamDisplayLabel(PARAM_SIMPLE)));
//        paramList.add(new ParameterDefinitionImpl(
//                PARAM_FILE_COUNT,
//                DataTypeDefinition.INT,
//                false,
//                "Files Count"));
//        paramList.add(new ParameterDefinitionImpl(
//                PARAM_FOLDER_COUNT,
//                DataTypeDefinition.INT,
//                false,
//                "Folders Count"));
    }

    @Override
    protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
        // Get parameter values
        String simpleParam = (String) action.getParameterValue(PARAM_SIMPLE);
//        Integer filesCount = (Integer) action.getParameterValue(PARAM_FILE_COUNT);
//        Integer foldersCount = (Integer) action.getParameterValue(PARAM_FOLDER_COUNT);


        LOG.info("Simple Repo Action called from scheduled Job, [" + PARAM_SIMPLE + "=" + simpleParam + "]");
//        LOG.info("actionedUponNodeRef: " + actionedUponNodeRef);
//        LOG.info("actionedUponNodeRef.getId: " + actionedUponNodeRef.getId());
//        LOG.info("action.getActionDefinitionName: " + action.getActionDefinitionName());

        NodeService nodeService = serviceRegistry.getNodeService();
        Map<QName, Serializable> properties = nodeService.getProperties(actionedUponNodeRef);
//        int filesCountOld = (int) nodeService.getProperty(actionedUponNodeRef,
//                QName.createQName(Task1Model.ASPECT_TASK1_FILES_COUNT));
//        int foldersCountOld = (int) nodeService.getProperty(actionedUponNodeRef,
//                QName.createQName(Task1Model.ASPECT_TASK1_FOLDERS_COUNT));

        if (nodeService.exists(actionedUponNodeRef) == true) {
            // The implementation of the Repo Action goes here...
//            LOG.info("actionedUponNodeRef: " + actionedUponNodeRef);
            String nodeName = (String)serviceRegistry.getNodeService().getProperty(
                    actionedUponNodeRef, ContentModel.PROP_NAME);
            Counts counts = new Counts();
            calculateCounts(actionedUponNodeRef, counts);
            int filesCountNew = counts.filesCount;
            int foldersCountNew = counts.foldersCount;
            LOG.info("filesCount: " + filesCountNew);
            LOG.info("foldersCount: " + foldersCountNew);
            LOG.info("Simple Repo Action invoked on node [name=" + nodeName + "]");


//            properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FILES_COUNT), filesCountNew);
//            properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FOLDERS_COUNT), foldersCountNew);

            // if the aspect has already been added, set the properties
            if (nodeService.hasAspect(actionedUponNodeRef,
                    QName.createQName(
                            Task1Model.NAMESPACE_TASK1_CONTENT_MODEL,
                            Task1Model.ASPECT_TASK1_FILES_COUNT))) {
            properties = nodeService.getProperties(actionedUponNodeRef);
            properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FILES_COUNT), filesCountNew);
            properties = nodeService.getProperties(actionedUponNodeRef);
            properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FOLDERS_COUNT), foldersCountNew);
//
                nodeService.setProperties(actionedUponNodeRef, properties);
            } else {
                // otherwise, add the aspect and set the properties
                if (LOG.isDebugEnabled()) LOG.debug("Node does not have aspect");

                properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FILES_COUNT), filesCountNew);
                nodeService.addAspect(actionedUponNodeRef,
                        QName.createQName(
                                Task1Model.NAMESPACE_TASK1_CONTENT_MODEL,
                                Task1Model.ASPECT_TASK1_FILES_COUNT), properties);
                properties.put(QName.createQName(Task1Model.ASPECT_TASK1_FOLDERS_COUNT), foldersCountNew);
                nodeService.addAspect(actionedUponNodeRef,
                        QName.createQName(
                                Task1Model.NAMESPACE_TASK1_CONTENT_MODEL,
                                Task1Model.ASPECT_TASK1_FOLDERS_COUNT), properties);

            }
        }
    }

    /**
     * @param type
     * @param nodeRef
     * @return true if node type equals or inherit from type <code>type</code>
     */
    protected boolean hasSubType(QName type, NodeRef nodeRef) {
//        LOG.info("Checking if node " + nodeRef.toString() + " has subtype " + type.toString());
        NodeService nodeService = serviceRegistry.getNodeService();
        List<QName> subTypes = new ArrayList<>();
        subTypes.addAll(dictionaryService.getSubTypes(type, true));
        QName nodeType = nodeService.getType(nodeRef);
        return nodeType.equals(type) || subTypes.contains(nodeType);
    }

    /**
     * @param actionedUponNodeRef
     * @param counts  The result object contains the counts of files and folders
     * @return number of files in the nodeRef folder
     */
    private void calculateCounts(NodeRef actionedUponNodeRef, Counts counts) {
        NodeService nodeService = serviceRegistry.getNodeService();

//        LOG.info("Counting files in nodeRef: " + actionedUponNodeRef);
        if (hasSubType(ContentModel.TYPE_FOLDER, actionedUponNodeRef)) {
            List<ChildAssociationRef> children = nodeService.getChildAssocs(actionedUponNodeRef, ContentModel.ASSOC_CONTAINS, RegexQNamePattern.MATCH_ALL);
            for (ChildAssociationRef child : children) {
                NodeRef childNode = child.getChildRef();
                if (hasSubType(ContentModel.TYPE_CONTENT, childNode)) {
                    counts.filesCount++;
                } else if (hasSubType(ContentModel.TYPE_FOLDER, childNode)) {
                    counts.foldersCount++;
                    calculateCounts(childNode, counts);
                }
            }
        }
    }
}
